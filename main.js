var config = {
  type: Phaser.WEBGL,
  width: window.innerWidth,
  height: window.innerHeight,
  physics: {
    default: 'arcade',
      arcade: {
        gravity: { y: 0 },
        debug: false
      }
  },
  scene: {
    preload: preload,
    create: create,
    update: update,
    extend: {
      player: null,
      enemy: null,
    }
  }
};

var game = new Phaser.Game(config);

function preload(){
  this.load.image('player', 'assets/player.png');
  this.load.image('enemy', 'assets/enemy.png');
}

function create(){
  //player
  player = this.physics.add.sprite(80, 60, 'player');
  player.setScale(.5);

  //enemy
  enemy = this.physics.add.sprite(400, 300, 'enemy');
  enemy.setScale(.5);

}

function update(){
  //set pointer
  pointer = this.input.activePointer;

  //player update
  player.rotation = Phaser.Math.Angle.Between(player.x,player.y,pointer.x,pointer.y);
  player.angle+=90;
  this.physics.moveToObject(player,pointer, 600);

  //enemy update
  enemy.rotation = Phaser.Math.Angle.Between(enemy.x,enemy.y, player.x,player.y);
  enemy.angle-=90;
  this.physics.moveToObject(enemy, player, 400);
}
